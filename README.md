# Distributed Computing 

### Amdahl's law

<a href="https://ibb.co/WpKHwRY"><img src="https://i.ibb.co/Cs0wZST/image.png" alt="image" border="0"></a>

 Amdahl’s law gives the theoretical speed-up when multiple processors are used. It is formulated as follows:

 <a href="https://imgbb.com/"><img src="https://i.ibb.co/kDTgr7M/image.png" alt="image" border="0"></a>

 Where:
 * `S(p)` - theoretical speed-up
 * `p` - number of processing threads/resources across which the parallel task is split
 * `f` - __parallel__ fraction of the program
 * `(1 -f)` - __serial__ (non-parallel) fraction of the program

 For example, if if `f = 1` (100%), then the program execution is completely parallel, we have `S(p) = p` which means the speed-up is exactly linear with respect to the number of threads. 

 This is the ideal situation where the system benefits fully from the increase of resources. What if `f` is less than 1 (100%)?

 <a href="https://ibb.co/zQVgvb4"><img src="https://i.ibb.co/5Gh3HvR/image.png" alt="image" border="0"></a>

Even for small reduction of 0.01 (1%) in the parallel portion of the run, we see the theoretical speed-up quickly flattens when we increase the resources and almost no improvement is found after 500. For the 0.9 case, the speed-up tops out at around 10 Regardless how many processors you throw at the problem. 

### Basic termins

__Node__ - process, running on dedicated machine.

This termin take place from [graph theory](https://en.wikipedia.org/wiki/Graph_theory#:~:text=In%20mathematics%2C%20graph%20theory%20is,also%20called%20links%20or%20lines).
Bacicly, when two nodes have edge between, that means that two procceses can communicate with each other throw the network.

<a href="https://ibb.co/rxmL3X9"><img src="https://i.ibb.co/rxmL3X9/image.png" alt="image" border="0"></a>

__Cluster__ - collection of cumputers/nodes connected to each other. The nodes in a cluster are working on the same task (typically on running the same source code).

What if we have large and heavyweight task? 

<a href="https://ibb.co/zJxPNHx"><img src="https://i.ibb.co/dgMk7JM/image.png" alt="image" border="0"></a>

Can we split it throw nodes? And if yes, what part of the task is going to be perfomed by which node?

<a href="https://ibb.co/rGsMPtg"><img src="https://i.ibb.co/wNsCHLT/image.png" alt="image" border="0"></a>

The biggest benefit of distributed system is that we can parallelize the work and let each node work independently for that common goal. So, we could manually distribute the work and assign each node a separate task. 

<a href="https://ibb.co/HrtWPvd"><img src="https://i.ibb.co/brsSvhJ/image.png" alt="image" border="0"></a>

But that obviously will not scale. We could receive thousands of tasks per second, so we need a programmatic way to do that distribution.

### Manual Master Solution 
Instead we cam manually deside on one special node to be the __leader__ (or master). 

<a href="https://ibb.co/Q8MCKWB"><img src="https://i.ibb.co/hXVZmG5/image.png" alt="image" border="0"></a>

Master node will be in charge of distributing work and collecting the results.

<a href="https://ibb.co/MgfQw6v"><img src="https://i.ibb.co/WH0j8th/image.png" alt="image" border="0"></a>

This is better than spliting work manually, but anyway, each node can fail (including __master__ node). 
>In a large distribution system failer is not a question of "if", but a question of "when".

If our leader is not there to distribute the work/collect results - then entire cluster is decommissioned.

<a href="https://ibb.co/5vK5nRk"><img src="https://i.ibb.co/gw4DPRd/image.png" alt="image" border="0"></a>

### Automatic Leader selection
Solution - build and algorithm to allow the nodes to elect their own leader on demand. And make them all check the leaders health closely.

<a href="https://ibb.co/8D5ryQC"><img src="https://i.ibb.co/vxP3NMF/image.png" alt="image" border="0"></a>

If the master node become unavailable, then remaining nodes will reelect a new leader. 

### Challenges of Master-Worker architecture 
1. Automatic and System Leader selection - is no trivial task to solve
2. By default each node knows only about itself - [service registry](https://microservices.io/patterns/service-registry.html) and [discovery](https://www.nginx.com/blog/service-discovery-in-a-microservices-architecture/) is required.

One of the most famous high perfomance distribution system coordinator - [Apache Zookeeper](https://zookeeper.apache.org/).

#### What makes Zookeeper a good solution?
1. Zookeeper - distributed system itself that provides us high perfomance and availability 
2. Typicaly runs in a cluster of and odd numbers of nodes, higer that 3
3. Uses redundancy to allow failures and stay functional

<a href="https://ibb.co/wMNgW57"><img src="https://i.ibb.co/zGhxRqF/image.png" alt="image" border="0"></a>

### Abstraction model in Zookeeper 
Zookeeper use special virtual file system, that looks like [tree](https://zookeeper.apache.org/doc/current/zookeeperOver.html#sc_dataModelNameSpace).
>It exposes a simple set of primitives that distributed applications can build upon to implement higher level services for synchronization, configuration maintenance, and groups and naming. It is designed to be easy to program to, and uses a data model styled after the familiar directory tree structure of file systems. It runs in Java and has bindings for both Java and C. Coordination services are notoriously hard to get right. They are especially prone to errors such as race conditions and deadlock. The motivation behind ZooKeeper is to relieve distributed applications the responsibility of implementing coordination services from scratch.

#### Zookeeper file system
From docs:
>ZooKeeper allows distributed processes to coordinate with each other through a shared hierarchical namespace which is organized similarly to a standard file system. The namespace consists of data registers - called znodes, in ZooKeeper parlance - and these are similar to files and directories. Unlike a typical file system, which is designed for storage, ZooKeeper data is kept in-memory, which means ZooKeeper can achieve high throughput and low latency numbers.

<a href="https://imgbb.com/"><img src="https://i.ibb.co/pd7H78n/image.png" alt="image" border="0"></a><br /><a target='_blank' href='https://imgbb.com/'></a><br />

#### Data model and the hierarchical namespace
Each element at that tree (virtual file system) is called a __Znode__. 
Znode - hybrid between a file and a directory.

* Znodes can store data inside (like a file)
* Znodes can have children nodes (like a directory)

Also, there are two types of Znodes:
* Persistence (persists and stay between session)
* Ephemeral (deleted when session ends) 

<a href="https://imgbb.com/"><img src="https://i.ibb.co/8MT8fNR/image.png" alt="image" border="0"></a>

### Leader election
We can implement easy (dummy) leader election algorithm. And how to chose a leader? All candidates will create znode in zookeeper, if node create it first, then he will become a leader. Example:

<a href="https://ibb.co/LYR561Y"><img src="https://i.ibb.co/k5B10X5/image.png" alt="image" border="0"></a>

Let's create some Java code:
```
    private static final String ELECTION_NAMESPACE = "/election";

    /**
     * Create ephemeral znode for current candidate.
     *
     * @throws KeeperException
     * @throws InterruptedException
     */
    public void volunteerForLeadership() throws KeeperException, InterruptedException {
        String znodePrefix = ELECTION_NAMESPACE + "/candidate_";
        String znodeFullPath = zooKeeper.create(znodePrefix,
                new byte[]{},
                ZooDefs.Ids.OPEN_ACL_UNSAFE,
                CreateMode.EPHEMERAL_SEQUENTIAL);

        log.info("Znode name " + znodeFullPath);
        this.currentZnode = znodeFullPath.replace(ELECTION_NAMESPACE + "/", "");
    }

    /**
     * Chose leader by smallest id of znode.
     *
     * @throws KeeperException
     * @throws InterruptedException
     */
    public void choseLeader() throws KeeperException, InterruptedException {
        List<String> candidates = zooKeeper.getChildren(ELECTION_NAMESPACE, false);
        Collections.sort(candidates);

        if (candidates.size() == 0) {
            return;
        }

        String leader = candidates.get(0);

        if (this.currentZnode.equals(leader)) {
            log.info("I am the leader!");
            return;
        }

        log.info("I am not the leader, " + leader + " is the leader!");
    }
```

Let's image, that we have some code, whic running on each node, whrn application is starting. And then we elect leader by creating znode and chossing the smallest one.

For example, if we will start executing JARs of this application in 4 terminals, then we can see next result:

<a href="https://ibb.co/F43v4f6"><img src="https://i.ibb.co/b5mh5y1/image.png" alt="image" border="0"></a>

Leader - first terminal (node representation), because he was first, who created znode.

###  Parallel multiprocessor/multicomputer systems
Main difference between multiprocessor and multicomputer is that:
1. __Multiprocessor__ - is a computer system with two or more central processing units (CPUs) share full access to a common RAM. The main objective of using a multiprocessor is to boost the system’s execution speed, with other objectives being fault tolerance and application matching
2. A __multicomputer__ system is a computer system with multiple processors that are connected together to solve a problem. Each processor has its own memory and it is accessible by that particular processor and those processors can communicate with each other via an interconnection network.

__Multiprocessor__:

<a href="https://imgbb.com/"><img src="https://i.ibb.co/wdcqpD3/image.png" alt="image" border="0"></a>

There are also [two types](https://techdifferences.com/difference-between-uma-and-numa.html) of multiprocessor systems: _uniform memory access (UMA)_ and _non-uniform memory access (NUMA)_.

<a href="https://ibb.co/kgJcbcS"><img src="https://i.ibb.co/ynpsvsQ/image.png" alt="image" border="0"></a>

__Multicomputer__:

<a href="https://imgbb.com/"><img src="https://i.ibb.co/d4cWrtN/image.png" alt="image" border="0"></a>

### Sidecar pattern
The sidecar pattern is a single-node pattern made up of two containers. The first is the application container. It contains the core logic for the application. Without this container, the application would not exist. In addition to the application container, there is a sidecar container. The role of the sidecar is to augment and improve the application container, often without the application container’s knowledge. In its simplest form, a sidecar container can be used to add functionality to a container that might otherwise be difficult to improve. Sidecar containers are coscheduled onto the same machine via an atomic container group, such as the pod API object in Kubernetes. In addition to being scheduled on the same machine, the application container and sidecar container share a number of resources, including parts of the filesystem, hostname and network, and many other namespaces.

<a href="https://imgbb.com/"><img src="https://i.ibb.co/x3gBT92/image.png" alt="image" border="0"></a>

For example, we can start application container with another sidecar container for monitoring of CPU resources.

Let's use [dockerfile](https://gitlab.com/aleksandr.tamazian/mesh-group-task/-/blob/1bdb1618630b3ab88dee6b7101915768b24aa43d/Dockerfile) from my previous repository (spring boot application).

Build docker image:
```
$ docker build .
```

Then start container with `-d` flag for hash id of container.
```
$ docker container run -p 8010:8010 -d <imageId>
```

Then let's share containers via namespace and start sidecar with `top` utility:
```
docker run --pid=container:${APP_ID} \ -p 8080:8080 brendanburns/topz:db0fa58 /server --address=0.0.0.0:8080
```

Then go to `http://localhost:8080/topz`:

<a href="https://ibb.co/yg0Y90q"><img src="https://i.ibb.co/8PMdCM9/image.png" alt="image" border="0"></a>

### Redis
[Redis](https://redis.io/) - distributed NoSQL database for cashing.
As key-value storage, which use strings for key.

Strings are good choice because:
1) Unique
2) Binary safe
3) Key names can be up to 512 MB
4) Length versus Readability 

Here is some basics comands with [redis CLI](https://redis.io/topics/rediscli).

[Set command](https://redis.io/commands/set) - sets value by key:
```
SET key value [EX seconds] [PX milliseconds] [NX|XX]
```
* EX seconds - Set the specified expire time, in seconds.
* PX milliseconds - Set the specified expire time, in milliseconds.
* NX - Only set the key if it does not already exist.
* XX - Only set the key if it already exist.
* KEEPTTL - Retain the time to live associated with the key.

[Get command](https://redis.io/commands/get) - return value by key:
```
GET key
```

For example, let's define two users (Jane and Bob):
```
SET user:1 Jane
SET user:2 Bob
```
And then return them
```
GET user:1 
GET user:2
```

Here is some useful commands for searching all data by keys: `KEYS` and `SCAN`:
```
SCAN slot [MATCH pattern][COUNT count]
```

<a href="https://ibb.co/dBzkQtf"><img src="https://i.ibb.co/R2VQCNg/image.png" alt="image" border="0"></a>

[Delete command](https://redis.io/commands/del) is blocking command for deletion by key:
```
DEL key
```

### Redis TTL
Redis will keep the key in memory until space is required or is forced out by the eviction policy in force. 
[Expiration times](https://redis.io/commands/expire) can be set in milliseconds, seconds or a UNIX timestamp. 
The TTL can be set when the key is first created or can be set afterwards. 
The ability for Redis to automatically managed the retention of keys greatly simplifies the development and administration overhead. 
You don't need a separate code thread to clean up data after expiration times have been exceeded. 
You can remove the expiration time for a key. 

<a href="https://ibb.co/68xpPYM"><img src="https://i.ibb.co/yp1My09/image.png" alt="image" border="0"></a>

### Redis with Java

<a href="https://ibb.co/1M5z99C"><img src="https://i.ibb.co/bFZbrr4/image.png" alt="image" border="0"></a><br /><a target='_blank' href='https://imgbb.com/'>share picture</a><br />
